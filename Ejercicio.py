import math as m

class Incline_Plane:

    def calculate_movement(self,angle,cf,m1):
        calculation=[]
        m2=0
        movement=-m1*9.8*(m.sin(m.radians(angle))+cf*m.cos(m.radians(angle)))
        calculation.append([m2,"up"])
        while movement<0:
            m2+=0.5
            movement=m2*9.8-(m1*9.8*(m.sin(m.radians(angle))+cf*m.cos(m.radians(angle))))
            calculation.append([m2,"up"])
        calculation.append([m2,"down"])
        return calculation

    def calculate_angle(self,m1,m2,cf):
        answer=-1
        movement=0
        for i in range(75):
            movement=round(m2*9.8-(m1*9.8*(m.sin(m.radians(i+10))+cf*m.cos(m.radians(i+10)))),1)
            if movement==0:
                answer=i+10
                break
        return answer

Calculation=Incline_Plane()

print(Calculation.calculate_movement(55,0.15,6))
print(Calculation.calculate_angle(8,6,0.2))